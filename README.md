## Read pcap file using tcpdump docker container

**Command to run:**

    python3 capture_pcap.py --pcap-file=couchbase-lww.pcaps

**Usage**

$ python3 capture_pcap.py --help
usage: capture_pcap.py [-h] --pcap-file PCAP_FILE

optional arguments:
  -h, --help            show this help message and exit
  --pcap-file PCAP_FILE
                        path to pcap file
