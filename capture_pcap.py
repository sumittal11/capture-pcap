import docker
import os
import tarfile
import argparse
import sys
from io import BytesIO
import os
import time

# Make sure that docker daemon is running on system
# else uncomment below code or manuall start docker
#os.system('pgrep -f docker > /dev/null || dockerd')

parser = argparse.ArgumentParser()
parser.add_argument("--pcap-file", help="path to pcap file",
                    type=str, required=True)

args = parser.parse_args()

# check if given file exists or not
if not os.path.isfile(args.pcap_file):
    print("Error: file does not exists")
    sys.exit()

# validate the pcap file
if not args.pcap_file.endswith('.pcap'):
    print("Error: invalid file, it should have pcap extention")
    sys.exit()

client = docker.from_env()
image = 'corfr/tcpdump:latest'
found = 0

# pull the image if its not there
for image in client.images.list():
    if image == image.tags[0]:
        found = 1
if not found:
    client.images.pull('corfr/tcpdump')
    print("image pulled...")

# start the container for tcpdump image
container = client.containers.run("corfr/tcpdump:latest", detach=True)
container_name = container.name

# function to copy the host pcap file to docker container
def copy_to(src, dst):

    file_data = open(src, 'rb').read()
    name, dst = dst.split(':')
    print("container name : ", name)
    print("pcap file to analyse: ", dst)
    container = client.containers.get(name)

    pw_tarstream = BytesIO()

    pw_tar = tarfile.TarFile(fileobj=pw_tarstream, mode='w')
    tarinfo = tarfile.TarInfo(name=src)
    tarinfo.size = len(file_data)
    tarinfo.mtime = time.time()

    pw_tar.addfile(tarinfo, BytesIO(file_data))
    pw_tar.close()

    pw_tarstream.seek(0)

    success = container.put_archive(os.path.dirname(dst), pw_tarstream)

    if not success:
        raise Exception('Put file failed')
    return


dest = '/tmp/' + args.pcap_file
conatiner_dest = container_name + ':' + dest
copy_to(args.pcap_file, conatiner_dest)

# run tcpdump command to read the pcap file
print("read pcap file using tcpdump:")
tcpdump_cmd = 'tcpdump -XX -i eth0 -r ' + dest
exit_code, output = container.exec_run(tcpdump_cmd, stream=True)

# one of the way to displaying the pcap file using tcpdump
if not exit_code:
    for line in output:
        print(line.decode("utf-8"))

# remove container
container.stop()
container.remove()
